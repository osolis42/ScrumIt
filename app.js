const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const favicon = require('serve-favicon');
const bodyParser = require('body-parser');
const securityMiddleware = require('./middlewares/securityMiddleware');

// Routes declaration
const index = require('./routes/index');
const users = require('./routes/users');
const items = require('./routes/items');
const projects = require('./routes/projects');
const sprints = require('./routes/sprints');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Routes usage
app.use('/', index);
app.use('/users', /*securityMiddleware.verifyToken,*/ users);
app.use('/items', /*securityMiddleware.verifyToken,*/ items);
app.use('/projects', /*securityMiddleware.verifyToken,*/ projects);
app.use('/sprints', /*securityMiddleware.verifyToken,*/ sprints);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
