const express = require('express');

// Collection reference
const Item = require('../models/item');

// List items
function index(request, response, next) {
  const page = request.params.page ? request.params.page : 1;

  Item.paginate({}, {
    page: page,
    limit: 10
  }, (err, objs) => {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t read the Items   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'There you have the Items list!   ( ^-^)/',
        objs: objs
      });
    }
  });
}

// Create item
function create(request, response, next) {
  let item = new Item();

  item.name = request.body.name;
  item.description = request.body.description;
  item.validation = request.body.validation;
  item.priority = request.body.priority;
  item.value = request.body.value;
  item.inChargeBy = request.body.inChargeBy;
  item.estimatedHours = request.body.estimatedHours;
  item.remainingHours = item.estimatedHours;
  item.column = request.body.column;
  item.validated = request.body.validated;
  item.sprint = request.body.sprint;
  item.project = request.body.project;

  item.save((err, obj) => {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t save the Item   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'Item saved correctly!   ╰( ^ᴗ^)╯',
        objs: obj
      });
    }
  });
}

// Update item
function update(request, response, next) {
  let item = {};

  item.name = request.body.name;
  item.description = request.body.description;
  item.validation = request.body.validation;
  item.priority = request.body.priority;
  item.value = request.body.value;
  item.inChargeBy = request.body.inChargeBy;
  item.remainingHours = request.body.remainingHours;
  item.column = request.body.column;
  item.validated = request.body.validated;
  item.sprint = request.body.sprint;
  item.project = request.body.project;

  Item.findByIdAndUpdate(request.params.id, {$set: item}, function (err) {
    if (err) {
      response.json({
        error: true,
        message: 'Mmm... Couldn’t update the Item   ( ಠ_ಠ)7',
        objs: {}
      });
    } else {
      response.json({
        error: false,
        message: 'Item updated correctly!   (ﾉ ^ᴗ^)ﾉ',
        objs: item
      });
    }
  });
}

// Delete item
function remove(request, response, next) {
  const id = request.params.id;

  if (id) {
    Item.remove({
      _id: id
    }, function(err) {
      if (err) {
        response.json({
          error: true,
          message: 'Mmm... Couldn’t delete the Item   ( ಠ_ಠ)7',
          objs: {}
        });
      } else {
        response.json({
          error: false,
          message: 'Item deleted correctly!   (ﾉ ^ᴗ^)ﾉ',
          objs: {}
        });
      }
    });
  } else {
    response.json({
      error: true,
      message: 'Mmm... Couldn’t find the Item   ( ಠ_ಠ)7',
      objs: {}
    });
  }
}

module.exports = {
  index,
  create,
  update,
  remove
};
