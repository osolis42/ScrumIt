const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

// Schema definition
const itemSchema = Schema({
  name: String,
  description: String,
  validation: String,
  priority: {
    type: String,
    enum: ['Low', 'Medium', 'High']
  },
  value: {
    type: Number,
    enum: [0, 1, 2, 3, 5, 8, 13, 21, 34, 100]
  },
  inChargeBy: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },
  estimatedHours: {
    type: Number,
    enum: [1, 2, 4, 8, 16, 24, 40, 80, 240, 480, 720, 1440]
  },
  remainingHours: {
    type: Number,
    enum: [1, 2, 4, 8, 16, 24, 40, 80, 240, 480, 720, 1440]
  },
  column: {
    type: String,
    enum: ['Backlog', 'In progress', 'Review', 'Done'],
    default: 'Backlog'
  },
  validated: {
    type: String,
    enum: ['Yes', 'No']
  },
  sprint: {
    type: Schema.Types.ObjectId,
    ref: 'Sprint'
  },
  project: {
    type: Schema.Types.ObjectId,
    ref: 'Project'
  }
});

// mongoosePaginate usage
itemSchema.plugin(mongoosePaginate);

// Export Schema
module.exports = mongoose.model('Item', itemSchema);
