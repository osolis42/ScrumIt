const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate');

const Schema = mongoose.Schema;

// Schema definition
const projectSchema = Schema({
  projectName: String,
  description: String,
  requestDate: Date,
  startDate: Date
});

// mongoosePaginate usage
projectSchema.plugin(mongoosePaginate);

// Export Schema
module.exports = mongoose.model('Project', projectSchema);
