const express = require('express');
const router = express.Router();
const itemsController = require('../controllers/itemsController');

router.get('/:page?', itemsController.index);

router.post('/', itemsController.create);

router.put('/:id', itemsController.update);

router.delete('/:id', itemsController.remove);

module.exports = router;
