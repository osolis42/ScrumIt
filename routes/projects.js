const express = require('express');
const router = express.Router();
const projectsController = require('../controllers/projectsController');

router.get('/:page?', projectsController.index);

router.post('/', projectsController.create);

router.put('/:id', projectsController.update);

router.delete('/:id', projectsController.remove);

module.exports = router;
