const express = require('express');
const router = express.Router();
const sprintsController = require('../controllers/sprintsController');

router.get('/:page?', sprintsController.index);

router.post('/', sprintsController.create);

router.put('/:id', sprintsController.update);

router.delete('/:id', sprintsController.remove);

module.exports = router;
